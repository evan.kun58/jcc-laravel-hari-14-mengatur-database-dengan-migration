@extends('layout.master')
@section('judul')
Halaman Detail Cast {{$cast->id}}
@endsection

@section('content')

<h4 class="text-primary">{{$cast->nama}}</h4>
<p>{{$cast->umur}}</p>
<small>{{$cast->bio}}</small>

@endsection